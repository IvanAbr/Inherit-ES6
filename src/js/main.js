// Задания:
// 1. Добавить обработку toggle - на кнопку, чтобы ВСЕ  widget-ы открывались закрывались (есть в css класс close для этого) 
// 2. Создать на основе Widget функцию конструктор для picture добавить её шаблон внутрь (см пример текст), установить нужной src
// 3. Создать на основе Widget функцию конструктор для counter реализовать возможность инкремента, декремента с отображением соответсвующего значения
// вопросы в группу в skype


let widgets = [
    {
        id: 'text',
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, enim?'
    },
    {
        id: 'picture',
        src: 'img/kitten.jpg',
        width: 200,
        content:'11111',
        height: 150
    },
    {
        id: 'counter',
        value: 3
    }
];

let widgetTemplate = `
<div class="widget__preview">
    Block
</div>
<div class="widget__header">
    <button class="toggle-button">toggle</button>
</div>
<div class="widget__body">
    Body
</div>
`;

class Widget {
    constructor(option) {
        let id = option.id;
        this.el = document.createElement('div');
        this.el.innerHTML = widgetTemplate;
        this.el.id = id;
        this.preview = this.el.querySelector('.widget__preview');
        this.preview.innerText = id;
        this.container = this.el.querySelector('.widget__body');
        this.el.classList.add('widget');
        this.el.setAttribute('draggable', true);
        let handleDrag = this.handleDrag;
        this.el.ondragstart = handleDrag;
        this.header = this.el.querySelector('.toggle-button');
        let that = this;
        this.header.onclick = () => {
            that.el.classList.toggle('close');
        };
    }

     handleDrag(event) {
        event.dataTransfer.setData('text/plain', event.currentTarget.id)
    }
}

let counterTemplate = `
<div class="counter">
    <div class="counter__display">1</div>
    <div class="counter__control">
        <button class="decrement">-</button>
        <button class="increment">+</button>
    </div>
</div>
`;

let imageTemplate = `
<img  class="image" src="" alt=""/>
`;

let textTemplate = `
<p class="text"></p>
`;

class Text extends Widget {
    constructor(option) {
        super(option);
        this.container.innerHTML = textTemplate;
        this.container.querySelector('.text').innerText = option.content;
    }
}

class Picture extends Widget {
    constructor(option) {
        super(option);
        this.container.innerHTML = imageTemplate;
        this.container.querySelector('.image').src = option.src;
    }
}

class Counter extends Widget {
    constructor(option) {
        super(option);
        this.container.innerHTML = counterTemplate;
        this.container.querySelector('.counter');
        this.dec = this.container.querySelector('.decrement');
        let count = 1;
        let disp = this.container.querySelector('.counter__display');
        this.dec.onclick = () => {
            count--;
            disp.innerHTML = count;
        }

        this.inc = this.container.querySelector('.increment');
        this.inc.onclick = () => {
            count++;
            disp.innerHTML = count;
        }
    }
}

class Droppable {
    constructor(selector) {
        this.el = document.querySelector(selector);
        let handleDragEnter = this.handleDragEnter.bind(this);
        let handleDrop = this.handleDrop.bind(this);
        let handleDragLeave = this.handleDragLeave.bind(this);
        this.el.ondragover = handleDragEnter;
        this.el.ondrop = handleDrop;
        this.el.ondragleave = handleDragLeave;
    }

     add(widget) {
        this.el.appendChild(widget.el);
    }

    handleDrop(event) {
        let targetId = event.dataTransfer.getData("text");
        let dropped = document.getElementById(targetId);
        if (!dropped) return false;
        event.currentTarget.appendChild(dropped);
        this.el.style.border = 'none';
        return false;
    }

    handleDragEnter() {
        this.el.style.border = '4px solid lightgreen';
        return false;
    }

    handleDragLeave() {
        this.el.style.border = 'none';
    }
}

class createWigets {
    constructor(options) {
        return options.map((option) => {
            let widget;
            switch (option.id.toLowerCase()) {
                case 'text':
                    widget = new Text(option);
                    break;

                case 'picture':
                    widget = new Picture(option);
                    break;

                case 'counter':
                    widget = new Counter(option);
                    break;

                default: widget = new Widget(option);
            }
            return widget;
        });
    }
}

class addWigets {
    constructor(panel,widgets) {
        widgets.forEach((widget) => {
            panel.add(widget);
        });
    }
}

let controlPanel = new Droppable('#controlPanel');
let widgetsPanel = new Droppable('#widgetsPanel');
new addWigets(widgetsPanel, new createWigets(widgets));




